/**
 * Give at least three reasons why you should learn javascript.
 */

1. well paid
2. easy to learn
3. in demand

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

1. generic language for web dev
2. basic language to learn for all stack

/**
 * What is a variable? Give at least three examples.
 */

1.
Examples:
	1. let
	2. const
	3. var

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

===> String ex. "12345" "abcd"
	 Number ex. 6789 0.5
	 Boolean ex. true false


/**
 * Write the correct data type of the following:
 */

1. "Number" - string
2. 158 - number
3. pi - undefined
4. false - boolean
5. 'true' - string
6. 'Not a String' - string

/**
 * What are the arithmetic operators?
 */

 +, -, *, /

/**
 * What is a modulo? 
 */

% - gets the value of remainder

/**
 * Interpret the following code
 */

let number = 4;
number += 4;

Q: What is the value of number? 4

----

let number = 4;
number -= 4;

Q: What is the value of number? 0

----

let number = 4;
number *= 4;

Q: What is the value of number? 16

----

let number = 4;
number /= 4;

Q: What is the value of number? 1
